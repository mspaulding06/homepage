---
author: simonmar
title: "The GHC blog"
date: 2010-08-24
tags: 
---

As an experiment, we are moving the GHC blog from <http://ghcmutterings.wordpress.com/> to here.
