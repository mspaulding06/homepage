---
author: bgamari
title: "GHC Weekly News - 2015/07/21"
date: 2015-07-21
tags: 
---

Hi \*,

Welcome for the latest entry in the GHC Weekly News. Today GHC HQ met
to discuss the status of the imminent 7.10.2 release.

# 7.10.2 Status

In the past weeks we have been busily tracking down a number of regressions in
the `ghc-7.10` branch. At this point we have built up an impressive
[list](https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.2) of fixes.
Thanks to everyone who has helped in this process!

In addition to resolving a number of simplifier regressions and portability issues
(some mentioned in the Weekly News from 6 July), GHC 7.10.2 should be the first
release which works out-of-the-box with GHCJS, thanks to a fix from Luite Stegeman.
Moreover, we will have support for running in even the most minimal container
environment (#10623).

At this point we have successfully tested the pre-release against Stackage
(thanks for Michael Snoyman and Herbert Valerio Riedel for making this possible)
and have found no major issues. A source tarball will be finalized and sent to
the binary builders today. With luck we will have releasable binaries by the end of
the week.

# 7.11 Things

## Testing
Thomas Miedema has been doing some amazing work fixing some long-standing
validation failures on the ``master`` branch. Moreover, he has been examining
the viability of enabling
[larger swaths](https://phabricator.haskell.org/D1074) of the testsuite in
Harbormaster validations.

In addition, Thomas has been doing some great work smoothing over a variety of
rough edges in the build system and generally keeping things running smoothly.
Thanks Thomas!

## Typeable implementation
Another subject of discussion in this week's GHC call was
[Phabricator D757](https://phabricator.haskell.org/D757), a long-languishing
change which moves the generation of `Typeable` instances from types' use sites to
their definition sites. This change involves a trade-off as it moves compilation
effort to the defining module, which will adversely affect compilation times of
modules defining many types. Moreover, in the event that `Typeable` is never actually used
this time is wasted effort. That being said, the current design of generating
`Typeable` instances at the point of use makes the class a bit of a black sheep
at the moment.

## Runtime system matters
This week Simon Marlow will merge his [D524](https://phabricator.haskell.org/D524),
a rework of the runtime system's allocator which he reports has improved
performance significantly in his workloads. This commit splits the concerns of
reserving address space and requesting backing memory for this address space. While the former is relatively cheap, the latter can be quite expensive due to page-table setup.
Consequently, his rework allocates a large chunk of addressing space up front
and then incrementally commits it as needed. Interested readers are encouraged
to look at the patch, which offers a nice glimpse into the inner workings of
GHC's memory allocator.

Simon also has finished Phab:D1076, which should improve garbage collector performance by
reworking the logic responsible for scavenging static objects. This work will
be merged shortly.

Also discussed was the profiler mis-attribution issue mentioned in the
Weekly News from 6 July 2015 (#10007). Peter Wortmann is currently looking at this issue,
which ends up being due to an infelicity in the semantics implemented by the
runtime system. Simon Marlow expressed that he did not know of a way to resolve
this that isn't quite invasive. We will have to see what Peter proposes.

## Applicative `do`

For some time now Simon Marlow has been working on
[implementing](https://phabricator.haskell.org/D729) the [[ApplicativeDo]]
proposal. Today in the call we discussed the status of this work and concluded
that while some refactoring is likely possible, the work can be merged as-is.
Hopefully at some point in the next few weeks this work will land.

## Haddock comments for GADT constructors

It came to our attention that the GHC parser was unable to parse Haddock
comments attached to GADT constructors. As it turns out, this is a rather long-standing
problem. Despite this fact, the [fix](https://phabricator.haskell.org/D1086)
ended up being quite straightforward and will be in 7.12.

# Backwards Compatibility

In general one should be able to compile the current GHC `master` branch with
the last two compiler releases. Recently, however, the reality is a bit less
clear-cut: while the current `ghc-7.10` branch GHC will technically **build** with
GHC 7.6 and 7.8, the tree does not necessarily pass the `validate` script due
to a variety of imports rendered redundant by AMP and FTP. Moreover, the
official policy on backwards compatibility is rather well-hidden on the
[[Commentary/CodingStyle]] page.

This was discussed in today's meeting and it was decided that we will in the future
maintain full validation-compatibility with the previous two releases. To ease this
we will relax the use of `-Werror` when building the stage 1 compiler.

On a related note, this week Thomas Miedema
[ripped out](https://phabricator.haskell.org/D904) some `#ifdefs` for GHC 7.6
compatibility from the `master` branch. Be aware that you will now need GHC 7.8 or newer to build `master`.

# Mac OS X El Capitan support

It is well-known that the next Mac OS X release, El Capitan, will default to
"root-less" mode, a security feature which restricts the operations available to
even the `root` user. As a result of this feature some system calls in the El Capitan developer preview fail with
`EPERM` instead of the usual `EACCES`. This
change uncovered a bug in the `unix` library where `EPERM` was not treated
similarly to `EACCES`. This was
[fixed](https://github.com/haskell/unix/pull/18) in November 2014, a fix which
is included in GHC 7.10.1.

However, there have been a
[few calls](https://mail.haskell.org/pipermail/ghc-devs/2015-July/009403.html)
on `ghc-devs` for a bugfix release of the 7.8 series including the updated `unix`. We
discussed this in the call today and concluded that we would not make such a
release. Given that El Capitan is not yet released and the issue is fixed in 7.10, it doesn't seem worthwhile to put more developer time into 7.8. We would suggest that any El
Capitan user unable to update to GHC 7.10.1 or newer disable root-less mode for
the time being. This can be accomplished with,

```
sudo nvram boot-args="rootless=0"
```


# Infrastructure

Recently it came to our attention that one of the build machines used by
Harbormaster (Phabricator's build mechanism) was still running GHC 7.6. If you
have seen strange validation issues on Harbormaster builds in the past, this is may
be the cause. As of today this is fixed; all Harbormaster validations are now
being built with GHC 7.8.4.

Harbormaster has historically had trouble working with Differentials which
changed submodule revisions. This has made testing revisions involving submodules quite tricky. Thanks to a
[patch](https://github.com/haskell-infra/phab-ghc-builder/pull/2) from Adam Sandberg Eriksson Harbormaster can now grab submodule commits from non-upstream repositories if set in `.gitmodules`.

Herbert Valerio Riedel has been making great strides improving the
responsiveness of Trac. A Trac upgrade, a move to Postresql, and some fiddling
with the WSGI configuration should result in a much better experience for
everyone.

Have a great week!

~ Ben

