---
author: thoughtpolice
title: "GHC Weekly News - 2014/12/16"
date: 2014-12-16
tags: ghc news
---

Hi \*,
   
time for another piece of the GHC weekly news!

  - Joachim Breitner has gotten the new GHC 7.8.4 package to tentatively build
  on ARM quite easily for Debian. Austin also took the liberty of merging all
  the needed patches; they'll be part of the 7.8.4 release
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007608.html>

  - Greg Weber announced he's taken the time to set up a Docker image for GHC
  development - if you're on Linux, Greg's image should help you get up to
  speed with a GHC development environment in minutes!
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007606.html>

  - Lennart Kolmodin has spent time working on autocompletion for GHC, and 7.10
  will ship with bash completion scripts - which package maintainers and
  distributions can now ship for their users. Thank you Lennart!
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007614.html>

  - Adam Gundry has a question about the new type checker plugin
  infrastructure; in particular - how do we deal with the serialization of type
  checker evidence that plugins may want to create or pass around on their own?
  Richard, Simon and Iavor weigh in.
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007626.html>

  - For the past few days, Richard Eisenberg has been hunting a performance
  regression in the compiler. After profiling, discussion on IRC and elsewhere,
  Richard has finally made some headway, and discovered one of the 'hot spots'
  in his patch. Unfortunately the battle isn't quite over just yet, and the
  hunt for a few more % increase remains.
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007645.html>

  - David Spies has hit a very strange situation with GHC 7.8.3 running out of
  memory. But it turned out this was a change in 7.8, in relation to how stacks
  were managed. Phew!
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007646.html>

  - Austin made a final call for 7.8.4 bugfixes. He plans on making the final
  release this week, if nobody has any other major complaints.
  <https://www.haskell.org/pipermail/ghc-devs/2014-December/007684.html>

Finally, in a slight change, we'll also be covering some notes from this week's
meeting between GHC HQ (Austin, Simon PJ, SimonM, Herbert and Mikolaj),
including...

 - The 7.10 RC1 looks like it's scheduled to occur this week still; all of our
   libraries and submodules are up-to-date, and we've taken the time to alert
   all of our maintainers about this. Thanks to Herbert for taking control of
   this!

 - We'll soon be implementing a new 'push hook' for the `ghc.git` repository:
   no more trailing whitespace. Since we've recently detabbed, and de-lhs-ified
   the tree, a knock-on effect was deleting trailing whitespace. Now that we've
   done a lot of this, we should take the time to enforce it - so they can't
   slip back in.

- Austin will be landing Phab:D169 and Phab:D396 soon to get it into 7.10.1 RC1.

 - This week, Austin managed to secure two sponsors for GHC/Haskell.org. We've
   been given a wonderful set of ARM buildbots (running in the cloud!) and a
   new, incredibly powerful POWER8 machine to use (with over 170 hardware
   threads available, for scalability testing). Hooray for our friends at
   Online.net and RunAbove.com for helping us out!

Closed tickets this week include: #9871, #9808, #9870, #9605, #9874, #9872, #9090, #9404, #8240, #9567, #9566, #9583, #9117, #9882, #9884, #9372, #7942, #8951, #9817, #9620, #9336, #9523, #9552, #8988, #9390, #9415, #9371, #7143, #9563, #8778, #4428, #4896, #9393, #9169, #7015, #8943, #8621, #9132, #9857, #8024, #9831, and #9888.
