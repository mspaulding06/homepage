---
author: bgamari
title: "GHC 8.8.4 released"
date: 2020-07-15
tags: release
---

The GHC team is proud to announce the release of GHC 8.8.4. The source
distribution, binary distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.8.4).

Release notes are also [available][1].

This release fixes a handful of issues affecting 8.8.3:

 - Fixes a bug in process creation on Windows (#17926). Due to this fix
   we strongly encourage all Windows users to upgrade immediately.
 
 - Works around a Linux kernel bug in the implementation of timerfd
   (#18033)
 
 - Fixes a few linking issues affecting ARM
 
 - Fixes "missing interface file" error triggered by some uses of
   `Data.Ord.Ordering` (#18185)
 
 - Fixes an integer overflow in the compact-normal-form import
   implementation (#16992)
 
 - `configure` now accepts a `--enable-numa` flag to enable/disable
   `numactl` support on Linux.
 
 - Fixes potentially lost sharing due to the desugaring of left operator
   sections (#18151).
 
 - Fixes a build-system bug resulting in potential miscompilation by
   unregisteised compilers (#18024)
 
As always, if anything looks amiss do let us know.

[1]: https://downloads.haskell.org/ghc/8.8.4/docs/html/users_guide/8.8.4-notes.html
